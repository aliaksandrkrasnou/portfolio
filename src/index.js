import { render as renderHomepage } from "./pages/homepage";
import { render as renderProjectspage } from "./pages/projectspage";

let currentPage = "homepage";

function switchPage() {
  if (currentPage === "homepage") {
    currentPage = "projectspage";
    // call proejctspage.js file
    renderProjectspage();
  } else {
    currentPage = "homepage";
    // call homepage.js file
    renderHomepage();
  }
}

console.info(currentPage);
switchPage();

setTimeout(() => {
  switchPage();
  console.info(currentPage);
}, 2500);
